#include <QtTest>
#include <QCoreApplication>

// add necessary includes here
#include "Team.h"

class tc_team : public QObject
{
    Q_OBJECT

public:
    tc_team();
    ~tc_team();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

tc_team::tc_team()
{

}

tc_team::~tc_team()
{

}

void tc_team::initTestCase()
{

}

void tc_team::cleanupTestCase()
{

}

void tc_team::test_case1()
{

}

QTEST_MAIN(tc_team)

#include "ut_Team.moc"
