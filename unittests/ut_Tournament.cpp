#include <QtTest>
#include <QCoreApplication>

// add necessary includes here
#include "Tournament.h"

class tc_tournament : public QObject
{
    Q_OBJECT

public:
    tc_tournament();
    ~tc_tournament();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

tc_tournament::tc_tournament()
{

}

tc_tournament::~tc_tournament()
{

}

void tc_tournament::initTestCase()
{

}

void tc_tournament::cleanupTestCase()
{

}

void tc_tournament::test_case1()
{

}

QTEST_MAIN(tc_tournament)

#include "ut_Tournament.moc"
